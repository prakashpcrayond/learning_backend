let dotenv = require('dotenv');
dotenv.config();
let userModel = require("../models/User");
let Token = require('../token/index');
let bcrypt = require("bcryptjs");

const login = async (req, res) => {

    try {

        const { email, password } = req.body;

        const user = await userModel.findOne({ email });

        if (!user) {
            return res.status(404).send({ message: "user not found" });
        };

        let passwordIsValid = bcrypt.compareSync(
            password,
            user.password
        );

        if (!passwordIsValid) {
            return res.status(401).send({ message: "invalid password" });
        };

        let verifyToken = {
            _id: user?._id, email: user?.email, mobileNumber: user?.mobileNumber, userName: user?.userName
        };

        const token = Token.generateToken({ verifyToken });

        return res.status(200).send({ message: "login successfully", userDetails: user, token: token });


    } catch (err) {

        return res.status(401).send({ message: err.message || 'error' });

    }
};

module.exports = login;