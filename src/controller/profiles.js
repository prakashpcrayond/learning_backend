let dotenv = require('dotenv');
dotenv.config();
let userModel = require("../models/Profile");
let Token = require('../token/index')

const addprofile = async (req, res) => {

    try {

        let token = req.headers["x-access-token"];

        if (!token) {
            return res.status(401).send({ error: true, message: "No token provided" });
        }

        await Token.verifyToken(token);

        const userData = new userModel({ ...req.body });
        await userData.save();

        return res.status(200).send({ message: "added successfully", userDetails: userData });


    } catch (err) {

        return res.status(404).send({ message: err.message || 'error' });
    }
};

const getprofile = async (req, res) => {

    try {

        userModel.find({}, function (err, users) {
            return res.status(200).send({ data: users });
        });

    } catch (err) {

        return res.status(404).send({ message: err.message || 'error' });

    }
};

const updateprofile = async (req, res) => {

    try {

        const filter = { _id: req.body.id };

        const update = { ...req.body };

        let token = req.headers["x-access-token"];

        if (!token) {

            return res.status(401).send({ error: true, message: "no token provided" });
        }

        await Token.verifyToken(token);

        await userModel.findOneAndUpdate(filter, update);

        res.status(200).send({ message: 'updated' });


    } catch (err) {

        return res.status(404).send({ message: err.message || 'error' });

    }
};


const deleteprofile = async (req, res) => {

    try {

        const filter = { _id: req.body.id };

        await userModel.findOneAndDelete(filter);
        res.status(200).send({ message: 'deleted' });

    } catch (err) {
        return res.status(404).send({ message: err.message || 'error' });
    }
};

module.exports = { addprofile, getprofile, updateprofile, deleteprofile };