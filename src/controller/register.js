let dotenv = require('dotenv');
dotenv.config();
let userModel = require("../models/User");
let Token = require('../token/index');
let bcrypt = require("bcryptjs");

const register = async (req, res) => {

    try {

        const { email, password, mobileNumber, userName } = req.body;

        const bodyParser = {
            email, password: bcrypt.hashSync(password, 8), mobileNumber, userName
        }

        const user = await userModel.findOne({ $or: [{ email }, { mobileNumber }] })

        if (user?.email === email) {
            return res.status(401).send({ message: 'email is already exit' })
        };

        if (user?.mobileNumber === mobileNumber) {
            return res.status(401).send({ message: 'mobilenumber is already exit' });
        }

        const userData = new userModel(bodyParser);

        await userData.save();

        const toke_id = await userModel.findOne({ email });

        const token = Token.generateToken({ _id: toke_id?._id, email, mobileNumber, userName });

        return res.status(200).send({ message: "register successfully", userDetails: userData, token: token });

    } catch (err) {

        return res.status(401).send({ message: err.message || 'error' });
    }
};

module.exports = register;