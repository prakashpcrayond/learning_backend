const { Schema, model } = require("mongoose");
const { v4: uuidv4 } = require('uuid');

const userSchema = new Schema({
  _id: { type: String, default: () => uuidv4() },
  userName: {
    type: String,
    required: [true, 'Please enter the name!']
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: [true, 'Please enter the mail!'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address!'],
  },
  mobileNumber: {
    type: Number,
    required: [true, 'Please enter the mobile number!'],
  },
  password: {
    type: String,
    required: [true, 'Please enter the password!'],
  },

}, { timestamps: true });

module.exports = model("users", userSchema);