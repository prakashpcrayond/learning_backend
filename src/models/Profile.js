const { Schema, model } = require("mongoose");
const { v4: uuidv4 } = require('uuid');

const userSchema = new Schema({
    _id: { type: String, default: () => uuidv4() },
    name: {
        type: String,
        required: [true, 'Please enter the name!']
    },
    discription: {
        type: String,
        required: [true, 'Please enter the discription!'],
    },
    like: {
        type: Boolean,
        default: false
    },
    file: {
        type: String,
    }

}, { timestamps: true });

module.exports = model("profile", userSchema);