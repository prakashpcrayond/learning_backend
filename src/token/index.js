let dotenv = require('dotenv');
dotenv.config();
let jwt = require('jsonwebtoken');

const generateToken = (payload) => {
    try {

        let token = jwt.sign(payload, process.env.TOKEN_KEY, {
            expiresIn: '1d'
        })

        return token

    } catch (error) {

        return res.status(404).send({ message: error.message || 'error' });
    }
}

const verifyToken = (token) => {

    return new Promise((resolve, reject) => {
        try {

            let payload = jwt.verify(token, process.env.TOKEN_KEY)

            resolve(payload);

        } catch (error) {

            reject(error);

        }
    });

}

module.exports = { generateToken, verifyToken }
