let register = require("../controller/register");
let login = require("../controller/login");
let profile = require("../controller/profiles");

module.exports = app => {

    app.post("/login", login);

    app.post("/register", register);

    app.post("/add/profile", profile.addprofile);

    app.get("/get/profile", profile.getprofile);

    app.post("/update/profile", profile.updateprofile);

    app.post("/delete/profile", profile.deleteprofile);

};