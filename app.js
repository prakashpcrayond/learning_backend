let dotenv = require('dotenv');
dotenv.config();
let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let cors = require('cors');
const app = express();

const PORT = process.env.APP_PORT || 8080;

app.use(cors());

// mongodb://user:password@domain.com:port/dbname
mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    connectTimeoutMS: 5000,
}).then(() => {

    app.use(express.json({ limit: '50mb' }));

    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

    // parse application/json
    app.use(bodyParser.json());

    // router's
    require("./src/routes")(app);

    // root error message's
    app.use((err, e, res, m) => {
        res.json({
            error: 'Failed',
            message: err.message,
        });
    });

    app.listen(PORT, () => { console.log('Keep Move On...') });
});
